package com.tvash.routeoptimizer.trrrrrrr;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.tvash.routeoptimizer.RouteCallback;
import com.tvash.routeoptimizer.RouteLink;

import java.util.List;


public class MainActivity extends AppCompatActivity implements  View.OnClickListener, RouteCallback {

    private static final Integer LOCATION_PICKER_INTENT = 1337;
    private static final String TAG = MainActivity.class.getCanonicalName();

    RecyclerView mList;

    RelativeLayout mBottom;

    FloatingActionButton add;

    FloatingActionButton remove;

    Button submit;

    private UserInputRouteAdapter mAdapter;

    /**
     * Lifecycle
     **/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mList = findViewById(R.id.form_route_list);
        mBottom = findViewById(R.id.form_submit_container);
        add = findViewById(R.id.form_fab_add);
        remove = findViewById(R.id.form_fab_remove);
        submit = findViewById(R.id.form_submit);
        add.setOnClickListener(this);
        remove.setOnClickListener(this);
        submit.setOnClickListener(this);
        initialiseRouteList();
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.form_fab_add) {
            addLocation();

        } else if (i == R.id.form_fab_remove) {
            clearList();

        } else if (i == R.id.form_submit) {
            calculateRoute();

        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == LOCATION_PICKER_INTENT) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(this, data);
                mAdapter.addItem(place.getAddress().toString());
            } else {
                Snackbar.make(mBottom, R.string.error_location_could_not_been_added, BaseTransientBottomBar.LENGTH_LONG).show();
            }
        }
    }

    public void addLocation() {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            startActivityForResult(builder.build(this), LOCATION_PICKER_INTENT);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            Snackbar.make(mBottom, R.string.error_google_places_not_reachable, BaseTransientBottomBar.LENGTH_LONG).show();
        }
    }

    public void clearList() {
        mAdapter.clearItems();
        Snackbar.make(mBottom, R.string.notification_deleted_route, BaseTransientBottomBar.LENGTH_LONG).show();
    }

    public void calculateRoute() {
        if (mAdapter.getItemCount() > 3) {
           new RouteLink(MainActivity.this,mAdapter.getData());
        } else {
            Snackbar.make(mBottom, R.string.error_nothing_to_optimize, BaseTransientBottomBar.LENGTH_LONG).show();
        }

    }


    private void initialiseRouteList() {
        mAdapter = new UserInputRouteAdapter(this);
        mList.setLayoutManager(new LinearLayoutManager(this));
        mList.setAdapter(mAdapter);
        mList.setHasFixedSize(true);

    }

    @Override
    public void getOptimizedRoute(List<String> optimizedRoute, double distanceSaved) {
        Snackbar.make(mBottom, "success", BaseTransientBottomBar.LENGTH_LONG).show();
        Log.d(TAG,"getOptimizedRoute"+"  "+optimizedRoute.toString()+"   "+distanceSaved);
    }

    @Override
    public void onError(String error) {
        Snackbar.make(mBottom, "failed", BaseTransientBottomBar.LENGTH_LONG).show();
        Log.d(TAG,"getOptimizedRoute"+"  "+error);
    }
}
